## Personal i3 config for Ubuntu
Current Version: `1.1.2`

### Installation guide
1. Run `sudo apt update`
2. Run `sudo apt install i3 rofi lxappearance feh x11-xserver-utils scrot imagemagick dbus flameshot xbacklight simplescreenrecorder sox libsox-fmt-all fonts-font-awesome autoconf blueman`.
3. Copy this i3 folder to .config folder in home directory (if it already exists, replace it).
4. Clone and install i3blocks from github (not apt(!)).
5. Run `ip link show` and set your network interface names (wifi + ethernet) in i3blocks.conf.
6. Check your battery name (BAT0 / BAT1) with ``upower -i `upower -e | grep 'BAT'` | grep 'native-path'`` and replace it in blocks/battery.sh if necessary.
7. Restart your pc. On login screen click on the settings icon and choose i3.
8. Modify your default UI design settings with lxappearance.
9. Copy some wallpaper images to wallpapers folder (the name of the files does not matter).
10. Select a lock screen image and copy it to lockscreen folder as image.png.
11. Run `sudo visudo` and add the following line at the end of the file: `<user> ALL=(ALL) NOPASSWD: /home/<user>/pathToConfig/blocks/brightness.sh`.
12. Navigate into the blocks folder and run `sudo chown root brightness.sh`.

### Further Information
- Restart i3 with $mod+Shift+r in order to apply any change.
- Always consider rebooting your PC if something does not work.

### References
- Visit http://i3wm.org/docs/userguide.html for a complete i3 reference.
- Visit https://i3wm.org/i3status/manpage.html for a complete i3status reference.
- Visit https://www.reddit.com/r/unixporn/comments/3358vu/i3lock_unixpornworthy_lock_screen/ for code and more options regarding the lock screen (lock.sh).
