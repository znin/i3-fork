#!/bin/bash

battery_level=$((`cat /sys/class/power_supply/${INTERFACE}/capacity`))
battery_status=`cat /sys/class/power_supply/${INTERFACE}/status`

if [ "$battery_status" == "Charging" ]
then
        echo " ${battery_level}% "
else
        echo " ${battery_level}%"
fi

echo "" # used for short_text output
if [ "$battery_level" -le 10 ]
then
        echo "#FF8000"
        if [ "$battery_status" != "Charging" ]
        then
                play ./sounds/battery_low.mp3
        fi
fi
