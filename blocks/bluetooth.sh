#!/bin/bash

if [[ "${BLOCK_BUTTON}" -eq 1 ]]; then
  echo ""
  exec blueman-manager
fi

STATUS=$(bluetooth | grep -wo 'on')

if [[ "${STATUS}" = "on" ]]; then
  echo ""
fi