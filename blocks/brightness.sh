#!/bin/bash

brightness_file="/sys/class/backlight/intel_backlight/brightness"
current_brightness=`cat ${brightness_file}`
new_brightness=$current_brightness

mode=$1
if [[ "${BLOCK_BUTTON}" -eq 4 ]]; then
	mode="inc"
elif [[ "${BLOCK_BUTTON}" -eq 5 ]]; then
	mode="dec"
fi

if [[ $1 == "inc" && current_brightness -lt 1500 ]]; then
	new_brightness=$(("$current_brightness" + 50))
elif [[ $1 == "dec" && current_brightness -gt 50 ]]; then
	new_brightness=$(("$current_brightness" - 50))
fi

sudo echo $new_brightness > ${brightness_file}
brightness_in_percent=$(("$new_brightness" / 15))
echo " ${brightness_in_percent}%"
