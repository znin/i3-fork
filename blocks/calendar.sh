#! /bin/sh

echo " $(date +%d.%m.%Y)"

if [[ "${BLOCK_BUTTON}" -eq 1 ]]; then
	cal | rofi -dmenu -markup -hide-scrollbar -markup-rows -no-custom -width 13 -lines 8 -bw 1 -font "Deja Vu Sans Mono 12" -yoffset -410 -xoffset 835
fi