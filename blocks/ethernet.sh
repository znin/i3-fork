#!/bin/sh

state () {
  cat /sys/class/net/$INTERFACE/operstate
}

if [ "$(state)" == 'up' ]; then
  echo ""
fi