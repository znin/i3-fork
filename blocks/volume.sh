#!/bin/bash

for SINK in `pacmd list-sinks | grep 'index:' | cut -b12-`
do
	if [ $1 == "toggle" ]
	then
  		pactl set-sink-mute $SINK $1
	else 
		pactl set-sink-volume $SINK $1
	fi
done