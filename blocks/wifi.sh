#!/bin/bash

[[ ! -d /sys/class/net/${INTERFACE}/wireless ]] && exit
if [[ "$(cat /sys/class/net/${INTERFACE}/operstate)" != 'down' ]]; then
    echo ""
fi