# important variables
set $mod Mod4
set $font Deja Vu Sans Mono
set $fontsize 12
set $configPath ~/.config/i3
set $display1 eDP-1
set $display2 HDMI-1
set $barPosition top

set $bg-color 	         #2f343f
set $active-bg-color     #707070
set $inactive-bg-color   #2f343f
set $text-color          #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color     #E53935



# set font and font-size
font pango: $font $fontsize

# kill focused window
bindsym $mod+Shift+q kill

# start rofi
bindsym $mod+d exec rofi -show drun -font "$font $fontsize" -bw 1 -lines 5 -width 25 -show-icons True -hide-scrollbar True

# focus windows
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused windows
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (toggle split)
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# restart i3
bindsym $mod+Shift+r restart

# start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# poweroff
bindsym $mod+Shift+p exec poweroff

# reboot
bindsym $mod+Shift+o exec reboot

# logout
bindsym $mod+Shift+i exec i3-msg exit

# lock screen
bindsym $mod+Shift+l exec bash $configPath/lockscreen/lock.sh

# detect screens
bindsym $mod+Shift+m exec xrandr --auto

# mirror screen
bindsym $mod+Shift+n exec xrandr --output $display2 --same-as $display1

# double screen
bindsym $mod+Shift+b exec xrandr --output $display2 --right-of $display1

# take screenshot
bindsym $mod+Shift+s exec flameshot gui -p ~/Downloads

# volume controls
bindsym $mod+F1 exec bash $configPath/blocks/volume.sh -5%
bindsym $mod+F2 exec bash $configPath/blocks/volume.sh toggle
bindsym $mod+F3 exec bash $configPath/blocks/volume.sh +5%

# spotify controls
bindsym $mod+Shift+g exec "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous"
bindsym $mod+Shift+h exec "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause"
bindsym $mod+Shift+j exec "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next"

# brightness controls
bindsym $mod+F4 exec sudo $configPath/blocks/brightness.sh dec
bindsym $mod+F5 exec sudo $configPath/blocks/brightness.sh inc


# resize window
bindsym $mod+r mode "resize"
mode "resize" {
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

# define workspace names
set $ws1 "1:Workspace 1"
set $ws2 "2:Workspace 2"
set $ws3 "3:Workspace 3"
set $ws4 "4:Workspace 4"
set $ws5 "5:Workspace 5"
set $ws6 "6:Workspace 6"
set $ws7 "7:Workspace 7"
set $ws8 "8:Workspace 8"
set $ws9 "9:Workspace 9"
set $ws10 "10:Workspace 10"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

hide_edge_borders both

# window colors
#                       border              background         text                 indicator
client.focused          $active-bg-color    $active-bg-color   $text-color          $bg-color
client.unfocused        $inactive-bg-color  $inactive-bg-color $inactive-text-color $bg-color
client.focused_inactive $inactive-bg-color  $inactive-bg-color $inactive-text-color $bg-color
client.urgent           $urgent-bg-color    $urgent-bg-color   $text-color          $bg-color

# bar settings
bar {
        # status_command i3status -c $configPath/i3status.conf
        status_command i3blocks -c $configPath/i3blocks.conf
        colors {
		        background $bg-color
	    	        separator #757575
		        #                  border             background         text
		        focused_workspace  $bg-color          $bg-color          $text-color
		        inactive_workspace $inactive-bg-color $inactive-bg-color $inactive-text-color
		        urgent_workspace   $urgent-bg-color   $urgent-bg-color   $text-color
	     }
       position $barPosition
       # prevent switching workspaces by clicking
       bindsym button1 nop
       # prevent switching workspaces by scrolling
       bindsym button4 nop
       bindsym button5 nop
       # order workspaces
       strip_workspace_numbers yes
       # disable program icons
       tray_output none
       # hide i3status bar
       # mode invisible
}

#-----startup-----#

# wallpaper
exec feh --randomize --bg-fill $configPath/wallpapers/*

# second screen
exec xrandr --output $display2 --right-of $display1

# bind workspaces to screens on startup
workspace $ws1 output $display1
workspace $ws2 output $display2

# define window properties
for_window [class="SimpleScreenRecorder"] floating enable;
for_window [class="Blueman-manager"] floating enable; sticky enable; move position mouse; resize shrink set 100px 250px;

# brightness
# bindsym XF86MonBrightnessUp exec xbacklight -inc 10 # increase screen brightness
# bindsym XF86MonBrightnessDown exec xbacklight -dec 10 # decrease screen brightness
# bindsym $mod+F4 exec --release bash $configPath/brightness.sh dec
# bindsym $mod+F5 exec --release bash $configPath/brightness.sh inc

#Todos
# -brightness
# -xrandr auto detection
# -fancy bar
# -config split files